Name:          efibootmgr
Release:       5
Version:       18
Summary:       A tool manipulating the EFI Boot Manager
License:       GPLv2+
URL:           https://github.com/rhboot/%{name}/
Source0:       https://github.com/rhboot/%{name}/archive/refs/tags/%{version}.tar.gz

Patch6000:     backport-Update-efibootmgr.c.patch
Patch6001:     backport-Add-missing-short-option-handling-for-index-I.patch
Patch6002:     backport-Fix-segfault-when-passed-index-is-greater-than-curre.patch
Patch6003:     backport-Fix-the-incorrect-long-parameter-in-help-messages.patch
Patch6004:     backport-efibootmgr-delete_bootnext-is-just-a-boolean-not-an-entry-id.patch

BuildRequires: gcc
BuildRequires: efi-srpm-macros >= 3-2 efi-filesystem git popt-devel efivar-libs >= 38-1 efivar-devel >= 38-1
Requires:      efi-filesystem
ExclusiveArch: %{efi}
Conflicts:     elilo <= 3.6-6
Obsoletes:     elilo <= 3.6-6

%description
Efibootmgr is a Linux user-space application to modify the Intel Extensible Firmware Interface (EFI)
Boot Manager. This application can create and destroy boot entries, change the boot order, change
the next running boot option, and more.

%package help
Summary: Documents for efibootmgr

%description help
Help package contains some readme, man and other related files for efibootmgr.

%prep
%autosetup -S git
git config --local --add %{name}.efidir %{efi_vendor}

%build
%make_build CFLAGS='%{optflags}' LDFLAGS='%{build_ldflags}'

%install
rm -rf %{buildroot}
%make_install libdir=%{_libdir} bindir=%{_bindir} mandir=%{_mandir} datadir=%{_datadir} \
              localedir=%{_datadir}/locale/ includedir=%{_includedir} libexecdir=%{_libexecdir}

%files
%{!?_licensedir:%global license %%doc}
%license COPYING
%{_sbindir}/*

%files help
%doc README
%{_mandir}/*/*.?.gz

%changelog
* Mon Jun 17 2024 zhangxingrong <zhangxingrong@uniontech.com> - 18-5
- Fix the incorrect long parameter in help messages
- efibootmgr: delete_bootnext is just a boolean, not an entry id

* Mon Mar 13 2023 zhangqiumiao <zhangqiumiao1@huawei.com> - 18-4
- Fix segfault when passed --index is greater than current boot order size
  Add missing short option handling for --index (-I)

* Mon Feb 20 2023 zhangqiumiao <zhangqiumiao1@huawei.com> - 18-3
- get_entry: return entry if it was found before reaching the end of the list

* Fri Nov 25 2022 zhangqiumiao <zhangqiumiao1@huawei.com> - 18-2
- Fix the wrong url of Source0 and modify the name of the source tarball

* Tue Oct 18 2022 zhangqiumiao <zhangqiumiao1@huawei.com> - 18-1
- update to verion 18

* Mon Apr 25 2022 yangcheng <yangcheng87@h-partners.com> - 17-2
- rebuild efibootmgr

* Mon Jun 21 2021 hanhui <hanhui15@huawei.com> - 17-1
- update to 17

* Tue Jun 8 2021 hanhui <hanhui15@huawei.com> - 16-8
- round community patches fix memory leak

* Wed May 26 2021 liuyumeng <liuyumeng5@huawei.com> - 16-7
- Add a BuildRequires for gcc

* Thu Sep 10 2020 hanzhijun <hanzhijun1@huawei.com> - 16-6
- solve source url problem

* Sun Sep 15 2019 yanzhihua <yanzhihua4@huawei.com> - 16-5
- Package init.
